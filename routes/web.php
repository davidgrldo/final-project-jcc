<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();



Route::middleware('auth')->group(function () {
    Route::get('/follow/{id}', 'UserController@follow')->name('follow');
    Route::get('/', 'HomeController@index')->name('home')->name('home');
    Route::get('/profile/{id}', 'HomeController@viewProfile')->name('profile'); //profile ini ada atau engga ya? profile dan user apakah sama?
    Route::get('/post/create', 'PostController@create');
    Route::get('/mypost/', 'PostController@index');
    Route::get('/post/{id}', 'PostController@show');
    Route::post('/post/', 'PostController@store');
    Route::get('/mypost/{id}/edit', 'PostController@edit');
    Route::put('/mypost/{id}/', 'PostController@update');
    Route::delete('/mypost/{id}/', 'PostController@destroy');
    Route::post('/post/{id}/comment', 'PostCommentController@store');
    Route::post('/post/{id}/like', 'LikePostController@like');
    Route::delete('/post/{id}/like', 'LikePostController@dislike');
    Route::post('/comment/{id}/like', 'LikeCommentController@like');
    Route::delete('/comment/{id}/like', 'LikeCommentController@dislike');
    Route::post('/comment/{id}/comment', 'ReplyCommentController@store');
    Route::resource('user', 'UserController');});
