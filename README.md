## Final Project

## Kelompok 11

## Anggota Kelompok
- **David Geraldo. (Email : davidgrldo123@gmail.com)**. Mengerjakan Database (Membuat ERD, migration, Auth dan beberapa crud fitur social media dan route).
- **Erlangga Juni Saputra. (Email : junisaputraerlangga@gmail.com)**. Mengerjakan Back end membuat fitur crud post dan sebagian besar crud fitur social media dan route.
- **Vicky Aditya Rifai. (Email : vickyadityarifai@gmail.com)**. Mengerjakan front end, menambah template, mengatur beberapa link agar terhubung dengan baik, membuat Readme.md, video penjelasan tugas akhir dan deploy aplikasi ke heroku.

## Tema Project
Social Media.

## ERD
<p align="center"><a href="https://gitlab.com/davidgrldo/final-project-jcc/-/raw/main/erd-final-jcc.png" target="_blank"><img src="https://gitlab.com/davidgrldo/final-project-jcc/-/raw/main/erd-final-jcc.png"></a></p>

## Link
- **Link Video :** https://drive.google.com/file/d/10Zn63-B6HFsK5lymbrJxdWCghO2gBb4E/view?usp=sharing
- **Link Deploy :** https://afternoon-gorge-85082.herokuapp.com/
