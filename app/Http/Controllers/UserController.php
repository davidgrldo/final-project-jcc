<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        return view('setting', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $request->validate([
            'password' => 'confirmed'
        ]);

        if (!empty($request->biodata)) {
            $user->update([
                'biodata' => $request->biodata
            ]);
        }
        if (!empty($request->password)) {
            $user->update([
                'password' => Hash::make($request->password)
            ]);
        }

        $user->update([
            'name' => $request->name,
            'updated_at' => Carbon::now()
        ]);

        return redirect()->route('user.index');
    }

    public function viewProfile($id)
    {
        $profile = User::findOrFail($id);
        $postCount = DB::table('posts')->where('user_id', $id)->count();
        return view('profile', compact('profile', 'postCount'));
    }

    public function follow($following_id)
    {
        $user = Auth::user();
        if ($user->following->contains($following_id)) {
            $user->following()->detach($following_id);
            $status = ['status' => 'unfollow'];
        } else {
            $user->following()->attach($following_id);
            $status = ['status' => 'follow'];
        }

        // return back();
        return response()->json($status);
    }
}
