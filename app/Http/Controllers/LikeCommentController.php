<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LikeComment;
use Illuminate\Support\Facades\Auth;

class LikeCommentController extends Controller
{
    public function like($id)
    {
        LikeComment::create([
            'comment_id' => $id,
            'user_id' => Auth::user()->id
        ]);
        return redirect()->back();
    }

    public function dislike($id)
    {
        $user_id = Auth::user()->id;
        $dislike = LikeComment::where('comment_id', '=', $id)
                            ->where('user_id', '=', $user_id)
                            ->get();
        $dislike->each->delete(); 
        return redirect()->back();
    }
}
