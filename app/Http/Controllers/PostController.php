<?php

namespace App\Http\Controllers;

use App\Post;
use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::where('user_id', Auth::user()->id)
            ->orderBy('created_at', 'desc')
            ->get();

        return view('post.mypost', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'content' => 'required',
            'gambar' => 'nullable'
        ]);

        $gambar = $request->file('gambar');
        $nama_gambar = "";
        if ($request->hasFile('gambar')) {
            $this->validate($request, [
                'gambar' => 'mimes:jpg,jfif,png,jpeg'
            ]);
            $extension = $gambar->getClientOriginalExtension();
            $nama_gambar = str_replace(' ', '-', strtolower(Auth::user()->name)) . '-' . date("d-m-Y-H-i-s") . '.' . $extension;
            $gambar->move('img/posts', $nama_gambar);
        }

        Post::create([
            'tulisan' => $request->content,
            'gambar' => $nama_gambar,
            'user_id' => $request->id
        ]);

        return redirect('/')->with('success', 'Postingan berhasil ditambahkan');
    }

    public function show($id)
    {
        $post = Post::find($id);
        return view('post.show', compact('post'));
    }

    public function edit($id)
    {
        $post = Post::find($id);
        return view('post.edit', compact('post'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'content' => 'required'
        ]);

        $post = Post::find($id);
        $post->tulisan = $request->content;
        $post->update();
        return redirect('/mypost');
    }

    public function destroy($id)
    {
        $user = Auth::user()->id;
        $post = Post::find($id);
        $post->delete();
        return redirect('/mypost');
    }
}
