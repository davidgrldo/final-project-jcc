<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ReplyComment;
use Illuminate\Support\Facades\Auth;

class ReplyCommentController extends Controller
{
    public function store($id, Request $request)
    {
        $this->validate($request,[
            'comment' => 'required'
        ]);

        ReplyComment::create([
            'comment_id' => $id,
            'user_id' => Auth::user()->id,
            'content' => $request->comment
        ]);
        return redirect()->back();
    }
    
}
