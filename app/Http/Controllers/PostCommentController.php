<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use Illuminate\Support\Facades\Auth;

class PostCommentController extends Controller
{
    public function store($id, Request $request)
    {
        $this->validate($request,[
            'comment' => 'required'
        ]);

        Comment::create([
            'post_id' => $id,
            'user_id' => Auth::user()->id,
            'content' => $request->comment
        ]);
        return redirect('post/'. $id);
    }

}
