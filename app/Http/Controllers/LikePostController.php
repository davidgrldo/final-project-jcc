<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LikePost;
use Illuminate\Support\Facades\Auth;

class LikePostController extends Controller
{
    public function like($id)
    {
        LikePost::create([
            'post_id' => $id,
            'user_id' => Auth::user()->id
        ]);
        return redirect()->back();
    }

    public function dislike($id)
    {
        $user_id = Auth::user()->id;
        $dislike = LikePost::where('post_id', '=', $id)
                            ->where('user_id', '=', $user_id)
                            ->get();
        $dislike->each->delete(); 
        return redirect()->back();
    }
}
