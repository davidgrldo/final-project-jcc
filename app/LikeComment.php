<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LikeComment extends Model
{
    protected $table="like_comment";
    protected $fillable = ['comment_id', 'user_id'];

    public function user()
    {
     return $this->belongsTo('App\User');
    }  
    public function comment()
    {
     return $this->belongsTo('App\Comment');
}
}