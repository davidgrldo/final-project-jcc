<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
  protected $table = "posts";
  protected $fillable = ["tulisan", "gambar", "user_id"];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function comment()
    {
        return $this->hasMany('App\Comment');
    }
    public function likepost()
    {
      return $this->hasMany('App\LikePost');
    }
    
}
