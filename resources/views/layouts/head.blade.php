<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<title>Social Media Kelompok 11</title>
<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
<link rel="icon" href="{{asset('aztemp/img/icon.ico')}}" type="image/x-icon" />

<!-- Fonts and icons -->
<script src="{{asset('aztemp/js/plugin/webfont/webfont.min.js')}}"></script>
<script>
    WebFont.load({
			google: {"families":["Open+Sans:300,400,600,700"]},
			custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands"], urls: ['../{{asset('aztemp/css/fonts.css')}}']},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
</script>

<!-- CSS Files -->
<link rel="stylesheet" href="{{asset('aztemp/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('aztemp/css/azzara.min.css')}}">
<link href="{{ asset('fontawesome/css/all.css') }}" rel="stylesheet">
@stack('style')
