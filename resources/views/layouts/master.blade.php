<!DOCTYPE html>
<html lang="en">

<head>
    @include('layouts.head')
</head>

<body>
    <div class="wrapper">
        <div class="main-header" data-background-color="purple">
            <!-- Logo Header -->
            <div class="logo-header">

                <div class="navbar-brand text-white">JCC LARAVEL</div>
            </div>
            <!-- End Logo Header -->

            @include('layouts.navbar')
        </div>

        @include('layouts.sidebar')
        <div class="main-panel">
            <div class="content">
                <div class="page-inner">
                    {{-- <div class="page-header">
                        <h4 class="page-title">Dashboard</h4>
                    </div> --}}
                    <div class="row">
                        <div class="col-md-8">
                        </div>
                        <div class="col-md-4">
                        </div>
                    </div>
                    <div class="row row-card-no-pd">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-head-row">
                                        <h4 class="card-title">
                                            @yield('judul-card')
                                        </h4>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        @yield('content')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    @include('layouts.scripts')
</body>

</html>
