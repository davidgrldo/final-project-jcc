@extends('layouts.master')
@push('style')
<link href="{{ asset('css/custom.css') }}" rel="stylesheet">
@endpush
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-4">
            <div class="card">
                <img class="profile-image" src="{{ asset('img/default-avatar.png') }}" alt="Card image" />
                <div class="card-body">
                    <h4 class="card-title text-center">{{ $profile->name }}</h4>
                    <div class="card-title text-center mb-2 mt-2">
                        @if (Auth::user()->id == $profile->id)
                        <a class="btn btn-info btn-sm text-white text-center"
                            href="{{ route('user.edit', $profile->id) }}">Edit
                            Profile</a>
                        @else
                        <a class="btn btn-primary btn-sm text-white text-center"
                            onclick="follow({{ $profile->id }}, this)">
                            {{ Auth::user()->following->contains($profile->id) ? 'Unfollow' : 'Follow' }}
                        </a>
                        @endif

                        <script>
                            function follow(id, el) {
                                    fetch('/follow/' + id)
                                    .then(respense => respense.json())
                                    .then(data => {
                                        el.innerText = (data.status == 'follow') ? 'Unfollow' : 'Follow'
                                    })
                                }
                        </script>
                    </div>
                    <div class="row">
                        <div class="col-4 text-center">{{ $postCount ? $postCount : 0 }} Postingan
                        </div>
                        <div class="col-4 text-center">{{ $profile->mengikuti ? $profile->mengikuti : 0 }} Mengikuti
                        </div>
                        <div class="col-4 text-center">{{ $profile->pengikut ? $profile->pengikut : 0 }} Pengikut</div>
                    </div>
                    <p class="text-center mt-2">{{ $profile->biodata }}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script')

@endpush
