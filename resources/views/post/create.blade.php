@extends('layouts.master')

@section('judul-card', 'Create Post')

@section('content')
<div class="row justify-content-center">
    <div class="col-8">
        <div class="card">
            <div class="card-body">
                <form action="/post" method="POST" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="id" value="{{ Auth::user()->id }}">
                    <div class="form-group">
                        <!-- <label for="content">Apa yang anda pikirkan?</label> -->
                        <textarea class="form-control" name="content" id="content"
                            style="width: 700px; height: 200px">Apa yang anda pikirkan?</textarea>
                        @error('content')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="image">Upload Foto</label>
                        <input accept=".png, .jpg, .jpeg, .gif, .jfif" type="file" class="form-control-file"
                            name="gambar" id="image">
                        @error('gambar')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary mx-2">Tambah</button>
                </form>
            </div>
        </div>
    </div>
    </form>

    @endsection
