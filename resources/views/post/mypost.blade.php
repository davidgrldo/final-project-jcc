@extends('layouts.master')

@section('judul-card')
    My Post <a href="/post/create" class="btn btn-secondary" type="button" id="createButton">Create New Post</a>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center mb-2">
        <div class="col-md-6">
        @forelse ($posts as $post)
            <div class="card">
                <div class="card-header">
                    <div class="dropdown">
                        <button class="btn btn-secondary" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Action
                        </button>
                        <div class="dropdown-menu">
                          <a class="dropdown-item" href="/mypost/{{$post->id}}/edit">Edit</a>
                          
                            <form action="/mypost/{{$post->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="dropdown-item" value="Delete">
                            </form>
                        </div>
                      </div>
                </div>
                <a href="/post/{{$post->id}}">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">{{$post->tulisan}}</div>
                    </div>
                    @if ($post->gambar != "")
                    <div class="row">
                        <div class="col-12">
                            <img src="{{asset('img/posts/'.$post->gambar)}}"
                                alt="posts" style="width: 100%; height:400px;">
                        </div>
                    </div>
                    @endif
                </a>
                    <div class="row">
                        <div class="col-6">
                            <form action="/post/{{$post->id}}/like" method="post" id="like">
                                @csrf
                                @forelse ($post->likepost as $like)
                                @method('DELETE')
                                <?php $class = "fas fa-heart";
                                      $liked = "Unlike";
                                ?>
                                @empty
                                <?php $class = "far fa-heart";
                                      $liked = "Like";
                                ?>
                                @endforelse
                                <button type="submit" class="btn btn-link"><i class="{{$class}}"></i> {{$post->likepost->count()}} {{$liked}}</button>
                                <a href="/post/{{$post->id}}"> <i class="far fa-comment"></i> {{$post->comment->count()}}  Comment</a>
    
                            </form>
                            {{-- <i class="fas fa-heart"></i> Liked --}}
                        </div>
                        <div class="col-6 text-right">
                            <span> {{$post->created_at->isoFormat('D MMMM Y') }} </span>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            @empty
            <center> Belum ada postingan</center>
                @endforelse      
        </div>
    </div>
</div>
@endsection
