@extends('layouts.master')
@section('judul-card', 'Manage Posts')
@section('button')
<a href="{{ route('posts.create') }}" class="btn btn-sm btn-primary" style="float: right">
    <i class="fas fa-plus"></i> Create Post
</a>
@endsection

@section('content')
<div class="row">
    @foreach ($posts as $post)
    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 d-flex align-items-stretch flex-column">
        <div class="card bg-light d-flex flex-fill">
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <img src="{{ asset('img/posts/'.$post->gambar) }}" alt="posts"
                            style="width: 100%; height:auto;">
                    </div>
                    <div class="col-12">
                        <p class="text-muted text-sm">
                            {{$post->tulisan}}
                        </p>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="text-right">
                    <form action="{{ route('posts.destroy', $post->id) }}" method="POST">
                        <a href="{{ route('posts.show', $post->id) }}" class="btn btn-sm btn-primary">
                            <i class="far fa-eye"></i> View
                        </a>
                        <a href="{{ route('posts.edit', $post->id) }}" class="btn btn-sm btn-info">
                            <i class="fas fa-pencil-alt"></i> Edit
                        </a>

                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i>Delete</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>
@endsection
