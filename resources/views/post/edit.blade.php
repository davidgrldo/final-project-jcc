@extends('layouts.master')

@section('judul-card')
        Edit Postingan
@endsection

@section('content')
<div class="row justify-content-center">
    <div class="col-8">
        <div class="card">
<!--             <div class="card-header">Postingan baru</div>
 -->            <div class="card-body">
                <form action="/mypost/{{$post->id}}" method="POST">
                    @csrf
                    @method('PUT')  
                    <div class="form-group">
                        <label for="content">Apa yang anda pikirkan?</label>
                        <textarea class="form-control" name="content" id="content" style="width: 700px; height: 200px"> {{$post->tulisan}} </textarea>
                        @error('content')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <button type="submit" class="btn btn-primary mx-2">Update</button>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
