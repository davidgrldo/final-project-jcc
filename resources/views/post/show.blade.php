@extends('layouts.master')
@section('content')

<div class="container">
    <div class="row justify-content-center mb-2">
        <div class="col-md-6">
            <div class="card">
                
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">{{$post->tulisan}}</div>
                    </div>
                    @if ($post->gambar != "")
                    <div class="row">
                        <div class="col-12">
                            <img src="{{asset('img/posts/'.$post->gambar)}}"
                                alt="posts" style="width: 100%; height:4    00px;">
                        </div>
                    </div>
                    @endif
                    <div class="row">
                        <div class="col-6">
                            <form action="/post/{{$post->id}}/like" method="post" id="like">
                                @csrf
                                @forelse ($post->likepost as $like)
                                @method('DELETE')
                                <?php $class = "fas fa-heart";
                                      $liked = "Unlike";
                                ?>
                                @empty
                                <?php $class = "far fa-heart";
                                      $liked = "Like";
                                ?>
                                @endforelse
                                <button type="submit" class="btn btn-link"><i class="{{$class}}"></i> {{$post->likepost->count()}} {{$liked}}</button>
                                <a href="/post/{{$post->id}}"> <i class="far fa-comment"></i> {{$post->comment->count()}}  Comment</a>
    
                            </form>
                        </div>
                        <div class="col-6 text-right">
                            <span> {{$post->created_at->diffForHumans() }} </span>
                        </div>
                    </div>
                    <br>
                    <h3>Komentar</h3>
                    @foreach ($post->comment as $item)
                    <div class="row">
                        <div class="col-6">
                            {{$item->user->name}}
                        </div>
                            <div class="col-6 text-right">
                                <span> {{$item->created_at->diffForHumans()}} </span>
                            </div>
                    </div>
                    <div class="row">
                            <div class="col-12">
                            {{$item->content}}
                        </div>
                    </div>
                        @foreach ($post->comment as $likecomment)
                        <div class="col-12">
                            <form action="/comment/{{$likecomment->id}}/like" method="post" id="like">
                                @csrf
                                @forelse ($likecomment->likecomment as $lc)
                                @method('DELETE')
                                <?php $class = "fas fa-heart";
                                      $liked = "Unlike";
                                ?>
                                @empty
                                <?php $class = "far fa-heart";
                                      $liked = "Like";
                                ?>
                                @endforelse
                                <button type="submit" class="btn btn-link"><i class="{{$class}}"></i> {{$likecomment->likecomment->count()}} {{$liked}}</button>
                                <i class="far fa-comment"></i> {{$likecomment->reply->count()}}  Comment</a>
                            </form>
                            Balasan
                            @foreach ($likecomment->reply as $reply)
                            <div class="row">
                            <div class="col-6">
                               
                                    <span> {{$reply->content}} </span>
                                </div>
                            </div>
                            @endforeach
                            <div class="col-12">
                                <form action="/comment/{{ $likecomment->id }}/comment" method="post">
                                    @csrf
                                    <div class="form-group">
                                <input type="text" class="form-control" name="comment" id="comment" placeholder="Balas Komentar">
                            </div>
                    <button type="submit" class="btn btn-primary mx-2">Balas</button>
                        </form>
                            @endforeach
                        </div>
                        
                            @endforeach
                               
    
                        </div>
                    
                            <div class="row">
                                <div class="col-12">
                                <form action="/post/{{ $post->id }}/comment" method="post">
                                    @csrf
                                    <div class="form-group">
                                <input type="text" class="form-control" name="comment" id="comment" placeholder="Tambahkan Komentar">
                            </div>
                    <button type="submit" class="btn btn-primary mx-2">Komentar</button>

                            </form>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>    
        </div>
 
@endsection
