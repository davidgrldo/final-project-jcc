@extends('layouts.master')
@push('style')
<link href="{{ asset('css/custom.css') }}" rel="stylesheet">
@endpush
@section('content')
<div class="container">
    <div class="row">
        <div class="col-4">
            <div class="card">
                <img class="profile-image" style="border: 1px solid #dfdfdf;"
                    src="{{ $user->avatar ? $user->avatar : 'img/default-avatar.png' }}" alt="Card image" />
                <div class="card-body">
                    <h4 class="card-title text-center">{{ $user->name }}</h4>
                    <div class="row">
                        <div class="col-6 text-center">{{ $user->mengikuti ? $user->mengikuti : 0 }} Mengikuti</div>
                        <div class="col-6 text-center">{{ $user->pengikut ? $user->mengikuti : 0 }} Pengikut</div>
                    </div>
                    <p class="text-center mt-2">{{ $user->biodata }}</p>
                </div>
            </div>
        </div>
        <div class="col-8">
            <div class="card">
                <div class="card-header">User Profile</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('user.update', $user->id) }}">
                        @csrf
                        @method('PUT')

                        <div class="form-group">
                            <label for="name">{{ __('Name') }}</label>
                            <input id="name" type="text" class="form-control form-control-sm " name="name"
                                value="{{ isset($user->name) ? $user->name : '' }}" autocomplete="name" autofocus>
                        </div>

                        <div class="form-group">
                            <label for="email">{{ __('E-Mail Address') }}</label>
                            <input class="form-control form-control-sm"
                                value="{{ isset($user->email) ? $user->email : '' }}" readonly>
                        </div>

                        <div class="form-group">
                            <label for="biodata">{{ __('Biodata') }}</label>
                            <textarea class="form-control form-control-sm"
                                placeholder="{{ isset($user->biodata) ? $user->biodata : '' }}" name="biodata"
                                rows="3"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="password">{{ __('Password') }}</label>
                            <input id="password" type="password" class="form-control form-control-sm" name="password"
                                autocomplete="new-password">
                        </div>

                        <div class="form-group">
                            <label for="password-confirm">{{ __('Confirm Password') }}</label>
                            <input id="password-confirm" type="password" class="form-control form-control-sm"
                                name="password_confirmation" autocomplete="new-password">
                        </div>

                        <div class="form-group mb-0">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Update') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
