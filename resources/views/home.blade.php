@extends('layouts.master')
@section('judul-card', 'Timeline')

@section('content')
<div class="container">
    <div class="row justify-content-center mb-2">
        <div class="col-md-6">
            @forelse ($posts as $post)
            <div class="card bg-light d-flex flex-fill mb-2">
                <div class="card-header" style="margin: initial !important;">
                    <a href="{{ route('profile', $post->user_id) }}">{{$post->user->name}}</a>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">{{$post->tulisan}}</div>
                    </div>
                    @if ($post->gambar != "")
                    <div class="row">
                        <div class="col-12">
                            <img src="{{ asset('img/posts/'.$post->gambar) }}" alt="posts"
                                style="width: 100%; height:auto;">
                        </div>
                    </div>
                    @endif
                    <div class="row">
                        <div class="col-6">
                           <form action="/post/{{$post->id}}/like" method="post" id="like">
                            @csrf
                            @forelse ($post->likepost as $like)
                            @method('DELETE')
                            <?php $class = "fas fa-heart";
                                  $liked = "Unlike";
                            ?>
                            @empty
                            <?php $class = "far fa-heart";
                                  $liked = "Like";
                            ?>
                            @endforelse
                            <button type="submit" class="btn btn-link"><i class="{{$class}}"></i> {{$post->likepost->count()}} {{$liked}}</button>
                            <a href="/post/{{$post->id}}"> <i class="far fa-comment"></i> {{$post->comment->count()}}  Comment</a>

                        </form>
                            {{-- <i class="fas fa-heart"></i> Liked --}}
                        </div>
                        <div class="col-6 text-right">
                            <span> {{ \Carbon\Carbon::parse($post->created_at)->diffForHumans() }} </span>
                        </div>
                    </div>
                </div>
                </a>
                
                <br>
            </div>
            @empty
            <center>No data</center>
            @endforelse
        </div>
    </div>
</div>
@endsection
